.. -*- restructuredtext -*-

============================================
Memo INFO-F404 *Real-Time Operating Systems*
============================================

**Memo**
about INFO-F404
*Real-Time Operating Systems*
(course by Joël Goossens, ULB, 2016)


* `PDF document`_
* `Booklet format in PDF`_
* `Booklet format in PostScript`_
* `LaTeX sources`_

.. _`Booklet format in PDF`: https://bitbucket.org/OPiMedia/memo-info-f404-real-time-operating-systems/raw/master/memo/INFO_F404_Real_Time_Operating_Systems_2016__memo__booklet.pdf
.. _`Booklet format in PostScript`: https://bitbucket.org/OPiMedia/memo-info-f404-real-time-operating-systems/raw/master/memo/INFO_F404_Real_Time_Operating_Systems_2016__memo__booklet.ps
.. _`LaTeX sources`: https://bitbucket.org/OPiMedia/memo-info-f404-real-time-operating-systems/src/master/memo/
.. _`PDF document`: https://bitbucket.org/OPiMedia/memo-info-f404-real-time-operating-systems/raw/master/memo/INFO_F404_Real_Time_Operating_Systems_2016__memo.pdf

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Memo|

(picture_ by David~O)

.. _picture: https://www.flickr.com/photos/8106459@N07/4468296467

.. |Memo| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/12/1969167606-5-memo-info-f404-real-time-operating-sy_avatar.png
