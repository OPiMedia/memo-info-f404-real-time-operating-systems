\chapter{Uniprocessor real-time scheduling}
Correctness:
logical result of computations
\textit{and} time of the result

\begin{itemize}
\item\index{real-time!Hard real-time}%
  \emphDef{Hard-real time}:
  catastrophic consequences
\item\index{real-time!Firm real-time}%
  \emphDef{Firm real-time}:
  some deadline may be missed
\item\index{real-time!Soft real-time}%
  \emphDef{Soft real-time}:
  deadline missed not important
\end{itemize}


\bigskip
\noindent
\index{job}%
\index{ Ji@$J_i$}%
\index{ a@$a$}%
\index{ e@$e$}%
\index{ d@$d$}%
\emphDef{Job $J_i = (a, e, d)$}
\begin{itemize}
\item
  $a$: release time
\item
  $e$: computer requirement (\#CPU unit must be received in $[a, d)$)
\item
  $d$: absolute deadline
\end{itemize}


\bigskip
\noindent
\index{task!periodic task}%
\index{ Ti@$\tau_i$}%
\index{ Oi@$O_i$}%
\index{ Ti@$T_i$}%
\index{ Di@$D_i$}%
\index{ Ci@$C_i$}%
\emphDef{Periodic task $\tau_i = (O_i, T_i, D_i, C_i)$}
\begin{itemize}
\item
  $O_i$ offset: release time of the \nth{1} job
\item
  $T_i$: period between 2 consecutive task releases
\item
  \index{deadline!relative deadline}%
  $D_i$ relative deadline: time delay between time release and absolute deadline
\item
  \index{computation time}%
  $C_i$ computation time: worst-case execution requirement
\end{itemize}

\smallskip
\noindent
Induce infinite sequence of jobs: $J_{i,1}, J_{i,2}, J_{i,3}, \dots, J_{i,k}, \dots$\\
$a = O_i + (k - 1) T_i$\hfil
$e = C_i$\hfil
$d = a + D_i$


\bigskip
\noindent
\index{task!sporadic task}%
\emphDef{Sporadic task}:
$T$ is the \textit{minimum} between 2 consecutive task release


\bigskip
\noindent
\index{system!periodic system}%
\index{system!sporadic system}%
\emphDef{Periodic}/\emphDef{sporadic system}:
finite set of periodic/sporadic tasks\hfill
$\tau = \{\tau_1, \dots, \tau_n\}$


\bigskip
\noindent
\index{utilization!task utilization}%
\emphDef{Task utilization $U(\tau_i)$} $\assign \frac{C_i}{T_i}$\hfil
\index{utilization!system utilization}%
\emphDef{System utilization $U(\tau)$} $\assign \sum\limits_{\tau_i \in \tau} U(\tau_i)$


\bigskip
\noindent
\begin{itemize}
\item
  \index{deadline!implicit deadline}%
  \emphDef{Implicit deadline}:
  $\forall i : D_i = T_i$
\item
  \index{deadline!constrained deadline}%
  \emphDef{Constrained deadline}:
  $\forall i : D_i \leq T_i$
\item
  \index{deadline!arbitrary deadline}%
  \emphDef{Arbitrary deadline}
\end{itemize}


\smallskip
\noindent
\begin{itemize}
\item
  \index{synchronous}%
  \emphDef{Synchronous}:
  $\forall i : O_i = 0$
\item
  \index{asynchronous}%
  \emphDef{Asynchronous}:
  not synchronous
\end{itemize}


\bigskip
\noindent
\begin{itemize}
\item
  \index{scheduling}%
  \emphDef{Scheduling discipline}:
  algo. distribute resources
\item
  \index{preemptive scheduler}%
  \emphDef{Preemptive scheduler}:
  capable interrupts process to allocate another
\end{itemize}


\bigskip
\noindent
\begin{itemize}
\item
  \index{FTP}%
  \index{Fixed Task Priority}%
  \emphDef{FTP Fixed Task Priority}:
  offline, one priority by task
\item
  \index{FJP}%
  \index{Fixed Job Priority}%
  \emphDef{FJP Fixed Job Priority}:
  online, one priority by job
\item
  \index{UD}%
  \index{Unrestricted Dynamic Priority}%
  \emphDef{DP Unrestricted Dynamic Priority}:
  priority may be change during lifetime of job
\end{itemize}


\bigskip
\noindent
Assumptions on this chapter:
\begin{itemize}
\item
  uniprocessor
\item
  hard real-time
\item
  preemptible tasks/jobs
\item
  \index{work-conserving}%
  \emphDef{work-conserving}:
  CPU cannot be idle if exist active jobs
\item
  tasks are independent (only CPU is shared)
\end{itemize}


\bigskip
\noindent
\index{response time}%
\begin{picture}(0,0)
  \put(270,-80){\includegraphics[height=5cm]{img/response_time}}
\end{picture}
\emphDef{Job response time} $=$ completion time - release time


\noindent
\setcounter{lemma}{5}%
\begin{lemma}
  $U(\tau) \leq 1
  \quad\Longleftarrow\quad
  \tau$ schedulable
\end{lemma}



\section{FTP}
\index{_priority@$\succ$}%
WLOG: $\tau_1 \succ \tau_2 \succ \tau_3 \succ \dots \succ \tau_n$\quad
($\simeq$sort, $O(n \log{n})$)


\subsection[RM Rate Monotonic]{\emphDef{RM Rate Monotonic}:
  lower period, higher priority}
\index{RM}%
\index{Rate Monotonic}%
For synchronous, \textit{implicit} deadline


\bigskip
\noindent
\begin{itemize}
\item
  \index{feasible!system FTP-feasible}%
  \emphDef{System FTP-feasible}:
  if exists schedulable FTP-priority assignment
\item
  \index{optimal!assignment FTP-optimal}%
  \emphDef{Assignment FTP-optimal}:
  if schedules \textit{all} FTP-feasible system
\end{itemize}


\bigskip
\noindent
\index{critical instant}%
\emphDef{Critical instant of $\tau_i$}:
instant s.t. job release at this instant has maximum response time of all jobs in $\tau_i$


\bigskip
\setcounter{lemma}{9}%
\begin{lemma}[\textsc{Liu}, 2000]\mbox{}\\
  $J_{i,k}$ release at same time $t$ job of every higher priority task\\
  \null\hfil
  $\Longleftarrow\quad$
  $t$ is critical instant of $\tau_i$
\end{lemma}


\bigskip
\begin{lemma}
  $\tau$ periodic,
  $S_1, S_2$ schedules of work-conserving schedulers for $\tau$\\
  $\forall t :\qquad S_1$ is idle at $t
  \quad\iff\quad
  S_2$ is idle at $t$
\end{lemma}


\bigskip
\setcounter{thm}{11}%
\begin{thm}
  synchronous,
  \textit{implicit} deadline\\
  RM is an FTP-optimal priority assignment
\end{thm}


\bigskip
\noindent
\index{ ri1@$r_i^1$}%
\emphDef{$r_i^1$}: response time of the \nth{1} request of $\tau_i$
\begin{thm}
  synchronous,
  \textit{constrained} deadline\\
  FTP-schedulable
  $\quad\iff\quad
  \forall i : r_i^1 \leq D_i$

  \smallskip
  \noindent
  \textsc{Audsley} \& \textsc{Tindell}, 1991:
  $r_i^1 = C_i + \sum\limits_{j=1}^{i-1} \big\lceil\frac{r_i^1}{T_j}\big\rceil C_j$\\
  Fixed point iteration (until $w_{k+1} = w_k$ or $w_k > D_i$):
  $\begin{array}[t]{@{}l@{ }l@{}}
    w_0 & \assign C_i\\
    w_{k+1} & \assign C_i + \sum\limits_{j=1}^{i-1} \big\lceil\frac{w_k}{T_j}\big\rceil C_j\\
  \end{array}$
\end{thm}


\bigskip
\begin{thm}[\textsc{Leung}, 2004]
  periodic, synchronous, implicit deadline\\
  $U(\tau) \leq n\,(\sqrt[n]{2} - 1)
  \quad\Longrightarrow\quad
  \tau$ feasible with RM
\end{thm}

\smallskip
\setcounter{corollary}{16}%
\begin{corollary}
  periodic, synchronous, implicit deadline\\
  $U(\tau) \leq \ln{}2 = 0.693\dots
  \quad\Longrightarrow\quad
  \tau$ feasible with RM
\end{corollary}


\bigskip
\noindent
$\begin{array}{l|r|r}
  & T_i = D_i & C_i\\
  \hline
  \tau_1 & 10 & 4\\
  \hline
  \tau_2 & 15 & 3\\
  \hline
  \tau_3 & 20 & 7
\end{array}$\qquad
not schedulable with RM


\subsection[DM Deadline Monotonic]{\emphDef{DM Deadline Monotonic}:
  lower relative deadline, higher priority}
\index{DM}%
\index{Deadline Monotonic}%
For synchronous, \textit{constrained} deadline\hfil
(RM $\subset$ DM)


\bigskip
\setcounter{thm}{15}%
\begin{thm}
  synchronous,
  \textit{constrained} deadline\\
  DM is an FTP-optimal priority assignment
\end{thm}


\bigskip
\noindent
$\begin{array}{l|r|r|r}
  & T_i & D_i & C_i\\
  \hline
  \tau_1 & 100 & 110 & 52\\
  \hline
  \tau_2 & 140 & 154 & 52
\end{array}$\hfil
\begin{tabular}{lll}
  RM: & $\tau_1 \succ \tau_2$ & failed\\
  DM: & $\tau_1 \succ \tau_2$ & failed\\
  but & $\tau_2 \succ \tau_1$ & success
\end{tabular}\\
\indent
$\Longrightarrow$
\begin{tabular}{l@{\ }|@{\ }}
  RM\\
  DM
\end{tabular}
are not optimal for arbitrary deadline


\bigskip
\noindent
\index{feasibility interval}%
\emphDef{Feasibility interval}:\\
finite interval s.t. if no deadline missing during this interval $\Rightarrow$ system feasible


\bigskip
\setcounter{thm}{17}%
\begin{thm}
  synchronous,
  \textit{constrained} deadline\\
  $[0, \max_i D_i)$ is a feasibility interval
\end{thm}


\bigskip
\noindent
\index{idle point}%
$x \in \naturals$ is an \emphDef{idle point}
if all requests occurring $< x$ are completed $\leq x$

CPU is idle at $x\quad\Rightarrow\quad x$ is an idle point


\bigskip
\noindent
\index{elementary busy period}%
$[a, b)$ is an \emphDef{elementary busy period} if
\begin{tabular}[t]{@{\ }|@{\ }l}
  $a, b$ are idle points\\
  idle point $\notin (a, b)$
\end{tabular}


\bigskip
\noindent
\index{level-i busy period@level-$i$ busy period}%
\emphDef{Level-$i$ busy period}:
\begin{tabular}[t]{@{\ }|@{\ }l}
  elementary busy period for $\{\tau_1, \tau_2, \dots, \tau_i\}$\\
  $\tau_i$ executes at least $1$ job
\end{tabular}


\bigskip
\setcounter{thm}{21}%
\begin{thm}
  synchronous:\\
  \begin{tabular}{|@{\ }l}
    largest response time for a request of $\tau_i \in$ \textit{first} level-$i$ busy period $[0, \lambda_i)$\\
    $\lambda_i$ is the first idle point $> 0$ for $\{\tau_1, \tau_2, \dots, \tau_i\}$\\
    $[0, \lambda_i)$ is the largest level-$i$ busy period
  \end{tabular}
\end{thm}


\bigskip
\begin{thm}
  synchronous, \textit{arbitrary} deadline:\\
  $[0, \lambda_n)$ is a feasibility interval

  \smallskip
  \noindent
  $\lambda_n$ is the smallest solution of
  $\lambda_n = \sum\limits_{i=1}^n \big\lceil\frac{\lambda_n}{T_i}\big\rceil C_i$\\
  Fixed point iteration:
  $\begin{array}[t]{@{}l@{ }l@{}}
    w_0 & \assign \sum\limits_{i=1}^n C_i\\
    w_{k+1} & \assign \sum\limits_{i=1}^n \big\lceil\frac{w_k}{T_i}\big\rceil C_i\\
  \end{array}$
\end{thm}


\subsection{Asynchronous}
\index{hyper-period}%
\index{ P@$P$}%
\emphDef{Hyper-period $P$} $\assign \text{lcm}_i T_i$\\
\index{ Omax@$O_{max}$}%
\emphDef{$O_{max}$} $\assign \max_i O_i$


\bigskip
\begin{thm}[\textsc{Leung} \& \textsc{Whitehead}, 1982]
  asynchronous, \textit{constrained} deadline:\\
  $[O_{max}, O_{max} + 2 P)$ is a feasibility interval
\end{thm}


\bigskip
\begin{thm}
  periodic task:\\
  feasibility schedules are periodic
\end{thm}


\bigskip
\begin{thm}
  asynchronous, \textit{constrained} deadline,
  $\tau_1 \succ \tau_2 \succ \dots \succ \tau_n$\\
  $\begin{array}[b]{@{}l@{ }l@{}}
    S_1 & \assign O_1\\
    S_i & \assign O_i + \big\lceil\frac{(S_{i-1} - O_i)^+}{T_i}\big\rceil T_i
  \end{array}$\hfill
  \index{ xplus@$x^+$}%
  \emphDef{$x^+$} $\assign \max \{0, x\}$\\
  Schedule is feasible up to $S_n + P$
  $\quad\Longrightarrow\quad$
  \begin{tabular}[t]{@{\ }|@{\ }l}
    is feasible\\
    periodic from $S_n$\\
    period $P$
  \end{tabular}\\
  $\Longrightarrow\quad$
  $[0, S_n + P)$ is a feasibility interval
\end{thm}



\bigskip
\noindent
$\begin{array}{l|r|r|r}
  & O_i & T_i = D_i & C_i\\
  \hline
  \tau_1 & 10 & 12 & 1\\
  \hline
  \tau_2 & 0 & 12 & 6\\
  \hline
  \tau_3 & 0 & 8 & 3
\end{array}$\hfil
\begin{tabular}{lll}
  RM, DM: & $\tau_3 \succ \tau_1 \succ \tau_2$ & failed\\
  but & $\tau_3 \succ \tau_2 \succ \tau_1$ & success
\end{tabular}\\
\indent
$\Longrightarrow$
\begin{tabular}{l@{\ }|@{\ }}
  RM\\
  DM
\end{tabular}
are not optimal for asynchronous



\subsection{\textsc{Audsley} algorithm}
$\tau_i$ is \emphDef{lowest-priority viable}
$\iff$
all jobs meet their deadline when\\
\null\hfill
\begin{tabular}[t]{@{\ }|@{\ }l@{}}
  $\tau_i$ has the lowest priority\\
  other tasks have any higher priority\\
  when scheduling $\tau_j \neq \tau_i$ consider deadline softs
\end{tabular}


\bigskip
\setcounter{thm}{27}%
\begin{thm}
  $\tau_i$ lowest-priority viable:\\
  $\exists$ feasible FTP-assignment for $\tau
  \quad\iff\quad
  \exists$ feasible FTP-assignment for $\tau \setminus \{\tau_i\}$
\end{thm}


\bigskip
\noindent
\index{Audsley algorithm@\textsc{Audsley} algorithm}%
\emphDef{\textsc{Audsley} algorithm}
\begin{lstlisting}[mathescape=true]
  procedure Audsley($\tau$) {
    if (there is no lowest-priority viable task) {
      return infeasible
    }
    else {
      $\tau_i \assign$ a lowest-priority viable task in $\tau$
      assign lowest priority to $\tau_i$
      Audsley($\tau \setminus \{\tau_i\}$)
    }
  }
\end{lstlisting}
Checking if a task is lower-priority viable on $[0, S_n + P_n)$\\
$O(n^2)$ distinct priority assignments\quad
$\ll O(n!)$



\section{FJP}
\index{Earliest Deadline First}%
\index{EDF}%
\emphDef{Earliest Deadline First EDF}:
lower \textit{absolute} deadline, higher priority


\bigskip
\noindent
\index{feasible!feasible job set}%
Job set $J$ is \emphDef{feasible}
if $\exists$ schedule which meets all the job deadlines


\bigskip
\setcounter{thm}{29}%
\begin{thm}
  Job set $J$ is feasible
  $\quad\Longrightarrow\quad$
  EDF feasibly schedule $J$\\
  EDF is
  \begin{tabular}[t]{@{\ }|@{\ }l}
    FJP-optimal\\
    FTP-optimal for
    \begin{tabular}[t]{@{\ }|@{\ }l}
      asynchronous\\
      abritrary deadline
    \end{tabular}
  \end{tabular}
\end{thm}


\bigskip
\begin{thm}
  Periodic, implicit deadline:\\
  $U(\tau) \leq 1
  \iff
  \tau$ schedulable
\end{thm}


\smallskip
\setcounter{corollary}{31}%
\begin{corollary}
  Periodic, implicit deadline:\\
  $U(\tau) \leq 1
  \iff
  \tau$ is EDF-schedulable
\end{corollary}


\bigskip
\setcounter{thm}{32}%
\begin{thm}\mbox{}\\
  \begin{tabular}{@{}l@{\ }|@{\ }l@{\ }|@{\ }}
    $\tau$ & synchronous\\
    & periodic\\
    & arbitrary deadline\\
    & EDF-feasible
  \end{tabular}
  $\Longrightarrow$
  \begin{tabular}{@{\ }|@{\ }l@{}}
    $\forall$ corresponding
    \begin{tabular}[t]{@{\ }|@{\ }l@{\ }|@{\,}}
      asynchronous\\
      periodic
    \end{tabular}:
    EDF-feasible\\
    $\forall$ corresponding sporadic:
    EDF-feasible
  \end{tabular}
\end{thm}


\bigskip
\noindent
$\begin{array}[b]{l|r|r}
  & T_i = D_i & C_i\\
  \hline
  \tau_1 & 4 & 2\\
  \hline
  \tau_2 & 7 & 3
\end{array}$\qquad
response time: $5, 5, 5, \mathbf{6}$
(the largest is not the first)


\bigskip
\setcounter{thm}{33}%
\begin{thm}[\textsc{Liu}, \textsc{Leyland}, 1973]
  periodic, synchronous, constrained deadline:
  deadline miss in $t$
  $\quad\Longrightarrow\quad
  \nexists$ idle $< t$
\end{thm}


\bigskip
\begin{thm}[\textsc{Goosens}, 1999]
  periodic, synchronous, constrained deadline:
  deadline miss in $t$
  $\quad\Longrightarrow\quad
  \nexists$ (except $0$) idle point $< t$

  \smallskip
  \noindent
  $\Longrightarrow [0, L)$ is a feasibility interval\hfil
  $L$ is the first idle point (except $0$)

  \smallskip
  \noindent
  $L\ (= \lambda_n) = $ smallest solution of
  $L = \sum\limits_{i=1}^n \big\lceil\frac{L}{T_i}\big\rceil C_i$
\end{thm}


\bigskip
\noindent
Periodic system $R$, schedule $S$:\\
\index{configuration}%
\index{ CSRt@$C_S(R, t)$}%
\emphDef{Configuration $C_S(R, t)$}
$\assign \big(\left(\gamma_1(t), \alpha_1(t), \beta_1(t)\right),
\dots,
\left(\gamma_n(t), \alpha_n(t), \beta_n(t)\right)\big)$\\
where
\begin{tabular}[t]{@{}|@{\ }l@{\ }l@{}}
  \index{ gammait@$\gamma_i(t)$}%
  \emphDef{$\gamma_i(t)$} & $\assign \left\{
    \begin{array}{@{\ }l@{\qquad}l@{}}
      (t - O_i) \% T_i & \text{if } t \geq O_i\\
      t - O_i & \text{if } t < O_i
    \end{array}\right.$\\
  \index{ alphait@$\alpha_i(t)$}%
  \emphDef{$\alpha_i(t)$} & $\assign$ \# active jobs of $\tau_i$\\
  \index{ betait@$\beta_i(t)$}%
  \emphDef{$\beta_i(t)$} & $\assign$ cumulative CPU time used by oldest active job of $\tau_i$
\end{tabular}


\bigskip
\setcounter{thm}{36}%
\begin{thm}
  $R$ periodic, asynchronous, arbitrary deadline, $S =$ EDF:\\
  $R$ is feasible
  $\iff$
  \begin{tabular}[t]{@{\ }|@{\ }l@{}}
    all deadline $\in [0, O_{max} + 2 P)$ met\\
    $C_S(R, O_{max} + P) = C_S(R, O_{max} + 2 P)$
  \end{tabular}
\end{thm}


\bigskip
\noindent
$\begin{array}{l|r|r|r|r||l}
  & O_i & T_i & D_i & C_i & U\\
  \hline
  \tau_1 & 0 & 4 & 4 & 2 & \sFrac{1}{2}\\
  \hline
  \tau_2 & 2 & 4 & 7 & 3 & \sFrac{3}{4}\\
  \hline
  & & & & & \sFrac{5}{4}
\end{array}$\hfil
\begin{tabular}{l}
all deadline $[0, 10)$ met\\
but $\beta_2(6) = 2 \neq \beta_2(10) = 1$
and $\tau_2$ misses at $t = 21$
\end{tabular}


\bigskip
\setcounter{corollary}{37}%
\begin{corollary}
  periodic, asynchronous, arbitrary deadline:\\
  $U(\tau) \leq 1
  \quad\Longrightarrow\quad
  [0, O_{max} + 2 P)$ is a feasibility interval
\end{corollary}



\section{DP}
Job $J = (a, e, d)$\\
\index{ epsilonJt@$\varepsilon_J(t)$}%
\emphDef{$\epsilon_J(t)$} $\assign$ cumulative CPU time used by $J$ since $a$.\\
\index{laxity}%
\index{ lJt@$l_J(t)$}%
\begin{picture}(0,0)
  \put(230,-30){\includegraphics[height=2cm]{img/laxity}}
\end{picture}
\emphDef{laxity $l_J(t)$} $\assign d - t - \big(e - \varepsilon_J(t)\big)$

\begin{tabular}{@{}l@{\ }l@{}}
  $> 0$: & $J$ can wait $l_J(t)$ time-units\\
  $= 0$: & $J$ must run immediately and until $d$\\
  $< 0$: & $J$ will miss
\end{tabular}


\bigskip
\noindent
$J$ running
$\quad\Longrightarrow\quad
l_J(t + 1) = l_J(t)$


\bigskip
\noindent
\index{LLF}%
\index{Least Laxity First}%
\emphDef{LLF Least Laxity First}


\bigskip
\setcounter{thm}{40}%
\begin{thm}[\textsc{Mok}, 1983]
  LLF is DM-optimal
\end{thm}


\bigskip
\noindent
$\begin{array}{l|r|r|r}
  & T_i & D_i & C_i\\
  \hline
  \tau_1 & 10 & 8 & 4\\
  \hline
  \tau_2 & 10 & 9 & 5
\end{array}$\hfil
\begin{tabular}{l}
  EDF: preemptive free\\
  LLF: $6$ preemptions by $10$ time-units
\end{tabular}


\bigskip
\noindent
\index{trashing}%
\emphDef{Trashing}:
large amount of resources are used to do a minimal work


\bigskip
\noindent
$\begin{array}{@{}l@{\ }|@{}}
  l_{J_1}(t) = l_{J_2}(t)\\
  \text{run }J_1
\end{array}
\quad\Longrightarrow\quad
l_{J_1}(t) = l_{J_2}(t) + 1$


\bigskip
\noindent
\begin{tabular}{@{}l@{\ }|@{\ }}
  $\exists$ FJP-optimal\\
  LLF cause lot of preemptions
\end{tabular}
so LLF useless
